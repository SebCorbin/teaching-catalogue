#!/usr/bin/env bash
cd /code/src
./manage.py migrate
./manage.py collectstatic
./manage.py compilemessages
