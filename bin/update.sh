#!/usr/bin/env bash
cd src
./manage.py migrate
./manage.py compilemessages
./manage.py collectstatic
cd -
