# Changelog

## Version 0.5dev

**New features**

**Improvements**

**Bug fixes**

## Version 0.4.1

**Improvements**

* Course detail in list

**Bug fixes**

* Fix course list only published by default

## Version 0.4

**New features**

* Search form on resource list

**Improvements**

* Add fields to read only API for organizations and resources
* French translation
* Add coverage in gitlab CI
* Add filters in admin page

**Bug fixes**

* Tag with space can be created on resource form

## Version 0.3

**New features**

* Add tags
* Add contributors
* When add or edit a resource it creates author or orga if not existing

**Improvements**

* Add more test and coverage
* Add gitlab CI

## Version 0.2

* Add city, address and contact_email to Organization
* Add level field to course materiel, FK to model Level
* Add title for homepage from settings or from site name
* Display three resources on home page
* Allow user to register
* Add command to import licenses from spdx json
* Add fixture for a few free licenses
* Update all packages

## Version 0.1

* Model for course material and organization
* Add license, author and organization to course material
* Anonymous can submit a resource
* Users can create an organization
* Create a readonly API
* Make app translatable
* Add tests
* Documentation for install and deploy
