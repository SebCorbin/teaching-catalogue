# Application settings

## Site title

Site title is displayed on homepage.

- first look at `SITE_TITLE` setting
    ```
    SITE_TITLE = "Your site title"
    ```
- then look at site name if available
- else display default
