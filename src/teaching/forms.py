# Forms here
from django import forms
from django_select2 import forms as s2forms
from django.utils.translation import gettext as _
from teaching.models import CourseMaterial, Author, Organization, Tag


class AuthorWidget(s2forms.ModelSelect2Widget):
    search_fields = [
        "name__icontains",
    ]


class OrganizationWidget(s2forms.ModelSelect2Widget):
    search_fields = [
        "name__icontains",
        "description__icontains",
    ]


class LicenseWidget(s2forms.ModelSelect2Widget):
    search_fields = [
        "code__icontains",
        "title__icontains",
    ]


class TagsWidget(s2forms.ModelSelect2TagWidget):
    queryset = Tag.objects.all()
    search_fields = [
        "label__icontains",
    ]

    def build_attrs(self, base_attrs, extra_attrs=None):
        base_attrs = {
            "data-token-separators": '[","]',
        }
        return super().build_attrs(base_attrs, extra_attrs=extra_attrs)

    def value_from_datadict(self, data, files, name):
        """Create objects for given non-pimary-key values. Return list of all primary keys."""
        values = super().value_from_datadict(data, files, name)
        if not values:
            return ''
        pk_values = [int(v) for v in values if v.isdigit()]
        pks = set()
        if pk_values:
            pks = self.queryset.filter(**{'pk__in': list(pk_values)}).values_list('pk', flat=True)
            pks = set(map(str, pks))
        cleaned_values = pk_values
        for val in set(values) - pks:
            cleaned_values.append(self.queryset.create(label=val).pk)
        return cleaned_values


class ContributorsWidget(s2forms.ModelSelect2TagWidget):
    queryset = Author.objects.all()
    search_fields = [
        "name__icontains",
    ]

    def value_from_datadict(self, data, files, name):
        """Create objects for given non-pimary-key values. Return list of all primary keys."""
        values = super().value_from_datadict(data, files, name)
        if not values:
            return ''
        pk_values = [int(v) for v in values if v.isdigit()]
        pks = set()
        if pk_values:
            pks = self.queryset.filter(**{'pk__in': list(pk_values)}).values_list('pk', flat=True)
            pks = set(map(str, pks))
        cleaned_values = pk_values
        for val in set(values) - pks:
            cleaned_values.append(self.queryset.create(name=val).pk)
        return cleaned_values


class CourseMaterialForm(forms.ModelForm):
    """CourseMaterial publish form"""

    new_author = forms.CharField(required=False,
                           label=_("Author name"),
                           help_text=_("Enter author name if not in the list"))

    new_orga = forms.CharField(required=False,
                         label=_("Organization name"),
                         help_text=_("Enter organization name if not in the list"))

    class Meta:
        model = CourseMaterial
        fields = ("title",
                  "description",
                  "level",
                  "tags",
                  "course_url",
                  "author",
                  "new_author",
                  "contributors",
                  "resource_license",
                  "organization",
                  "new_orga")
        widgets = {
            'author': AuthorWidget,
            'contributors': ContributorsWidget,
            'resource_license': LicenseWidget,
            'organization': OrganizationWidget,
            'tags': TagsWidget,
        }

    def clean_author(self):
        author_selected = self.cleaned_data['author']
        author_name = self.data.get('new_author', '')
        if author_name != "":
            new_author, created = Author.objects.get_or_create(name=author_name)
            return new_author
        return author_selected

    def clean_organization(self):
        orga_selected = self.cleaned_data['organization']
        orga_name = self.data.get('new_orga', '')
        if orga_name != "":
            new_orga, created = Organization.objects.get_or_create(name=orga_name)
            return new_orga
        return orga_selected


class CourseMaterialPublishForm(forms.ModelForm):
    """CourseMaterial publish form"""

    class Meta:
        model = CourseMaterial
        fields = ("status",)


class CourseMaterialSearchForm(forms.Form):
    """CourseMaterial search form"""

    q_term = forms.CharField(
        label=_("Search in title and description"),
        required=False
    )
    q_tags = forms.MultipleChoiceField(
        label=_("Search in tags"),
        widget=s2forms.ModelSelect2MultipleWidget(
            model=Tag,
            search_fields=['label__icontains'],
        ),
        required=False,
    )
    q_published = forms.BooleanField(
        label=_("Published"),
        required=False,
    )
