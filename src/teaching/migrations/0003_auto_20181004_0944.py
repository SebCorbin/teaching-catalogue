# Generated by Django 2.1 on 2018-10-04 09:44

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('teaching', '0002_auto_20180803_1508'),
    ]

    operations = [
        migrations.AlterField(
            model_name='coursematerial',
            name='author',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='courses', to='teaching.Author'),
        ),
    ]
