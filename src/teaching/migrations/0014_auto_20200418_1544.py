# Generated by Django 3.0.4 on 2020-04-18 15:44

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('teaching', '0013_auto_20200418_1427'),
    ]

    operations = [
        migrations.AlterField(
            model_name='coursematerial',
            name='level',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='teaching.Level', verbose_name='Niveau'),
        ),
    ]
