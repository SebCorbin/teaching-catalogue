# Generated by Django 3.1 on 2020-12-27 20:27

from django.db import migrations


def migrate_course_url(apps, schema_editor):
    """Migrate url data to course_url for all courses"""
    CourseMaterial = apps.get_model('teaching', 'CourseMaterial')
    for course in CourseMaterial.objects.all():
        course.course_url = course.url
        course.save()


class Migration(migrations.Migration):

    dependencies = [
        ('teaching', '0023_coursematerial_course_url'),
    ]

    operations = [
        migrations.RunPython(migrate_course_url),
    ]
