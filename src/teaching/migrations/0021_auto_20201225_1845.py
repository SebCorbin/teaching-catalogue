# Generated by Django 3.1 on 2020-12-25 18:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('teaching', '0020_auto_20201224_1441'),
    ]

    operations = [
        migrations.AddField(
            model_name='coursematerial',
            name='contributors',
            field=models.ManyToManyField(blank=True, related_name='contributed_courses', to='teaching.Author', verbose_name='Contributor'),
        ),
        migrations.AlterField(
            model_name='coursematerial',
            name='tags',
            field=models.ManyToManyField(blank=True, related_name='course_materials', to='teaching.Tag'),
        ),
    ]
