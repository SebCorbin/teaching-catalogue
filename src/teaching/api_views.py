from rest_framework import viewsets
from . import models, serializers


class CourseMaterialViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = models.CourseMaterial.objects.all()
    serializer_class = serializers.CourseMaterialSerializer


class OrganizationViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = models.Organization.objects.all()
    serializer_class = serializers.OrganizationSerializer
