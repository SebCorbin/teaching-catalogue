from django.contrib import admin
from teaching.models import Author, License, CourseMaterial, Organization, Level, Tag


class LicenseAdmin(admin.ModelAdmin):
    list_display = ('title', 'description')
    search_fields = ('title',)
    ordering = ('title',)


class TagAdmin(admin.ModelAdmin):
    list_display = ('label', 'description')
    search_fields = ('label',)
    ordering = ('label',)


class CourseMaterialAdmin(admin.ModelAdmin):
    list_display = ('title', 'author', 'resource_license', 'level', 'status')
    ordering = ('title',)
    search_fields = ('title', 'description')
    list_filter = ('status', 'level', 'tags')
    autocomplete_fields = ['resource_license', 'tags']


admin.site.register(License, LicenseAdmin)
admin.site.register(Author)
admin.site.register(Organization)
admin.site.register(Level)
admin.site.register(Tag, TagAdmin)
admin.site.register(CourseMaterial, CourseMaterialAdmin)
