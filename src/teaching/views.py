from django.urls import reverse_lazy
from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.sites.models import Site
from django.db.models import Q, QuerySet
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView
from django.utils.translation import gettext as _

from teaching.models import CourseMaterial, Organization, Author, PublicationStatus, Tag
from teaching.forms import CourseMaterialForm, CourseMaterialPublishForm, CourseMaterialSearchForm


class LastCourseList(ListView):
    """Homepage view"""
    model = CourseMaterial
    template_name = 'index.html'
    queryset = CourseMaterial.objects.filter(status=PublicationStatus.PUBLISHED).order_by('?')[:3]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        site_used = Site.objects.get(id=settings.SITE_ID)
        if hasattr(settings, 'SITE_TITLE') and settings.SITE_TITLE:
            context['homepage_title'] = settings.SITE_TITLE
        elif site_used:
            context['homepage_title'] = site_used.name
        return context


class CourseMaterialList(ListView):
    """CourseMaterial list view"""
    model = CourseMaterial

    def get_context_data(self, *, object_list=None, **kwargs):
        context_data = super().get_context_data(**kwargs)
        initial_data = {}
        if 'q_term' in self.request.GET and self.request.GET.get('q_term') != '':
            initial_data['q_term'] = self.request.GET.get('q_term')
        if 'q_tags' in self.request.GET and self.request.GET.get('q_tags') != '':
            initial_data['q_tags'] = self.request.GET.get('q_tags')
        if len(self.request.GET) == 0 or self.request.GET.get('q_published') == 'on':
            initial_data['q_published'] = True
        context_data['search_form'] = CourseMaterialSearchForm(initial=initial_data)
        return context_data

    def get_queryset(self):
        q_term = self.request.GET.get('q_term', '')
        queryset = CourseMaterial.objects.all()
        if self.request.GET.get('q_published') or len(self.request.GET) == 0:
            queryset = queryset.filter(status=PublicationStatus.PUBLISHED)
        if q_term:
            queryset = queryset.filter(
                Q(title__icontains=q_term) | Q(description__icontains=q_term)
            )
        q_tags = self.request.GET.get('q_tags', '')
        if q_tags:
            queryset = queryset.filter(tags__in=q_tags)
        return queryset


class CourseMaterialPendingList(ListView):
    model = CourseMaterial
    template_name = "teaching/coursematerial_pending_list.html"
    queryset = CourseMaterial.objects.exclude(status=PublicationStatus.PUBLISHED)

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['form'] = CourseMaterialPublishForm()
        return context


class CourseMaterialDetail(DetailView):
    """CourseMaterial detail view"""
    model = CourseMaterial


class CourseMaterialCreateView(CreateView):
    """CourseMaterial create view"""
    model = CourseMaterial
    form_class = CourseMaterialForm
    template_name = 'teaching/coursematerial_form.html'
    success_url = reverse_lazy('teaching:coursematerial-thanx')


class CourseMaterialUpdateView(PermissionRequiredMixin, UpdateView):
    """CourseMaterial update view"""
    model = CourseMaterial
    permission_required = "teaching.change_coursematerial"
    form_class = CourseMaterialForm
    template_name = 'teaching/coursematerial_form.html'
    success_url = reverse_lazy('teaching:coursematerial-list')


class CourseMaterialChangeStatusView(PermissionRequiredMixin, UpdateView):
    """CourseMaterial change status view"""
    model = CourseMaterial
    permission_required = "teaching.change_coursematerial"
    form_class = CourseMaterialPublishForm
    success_url = reverse_lazy('teaching:coursematerial-pending-list')


class OrganizationList(ListView):
    """Organization list view"""
    model = Organization


class OrganizationCreateView(LoginRequiredMixin, CreateView):
    """Organization create view"""
    model = Organization
    fields = ("name", "description", "website", "contact_email", "address", "city")
    template_name = 'teaching/organization_form.html'
    success_url = reverse_lazy('teaching:organization-list')


class OrganizationUpdateView(PermissionRequiredMixin, UpdateView):
    """Organization create view"""
    model = Organization
    permission_required = "teaching.change_organization"
    fields = ("name", "description", "website", "contact_email", "address", "city")
    template_name = 'teaching/organization_form.html'
    success_url = reverse_lazy('teaching:organization-list')
