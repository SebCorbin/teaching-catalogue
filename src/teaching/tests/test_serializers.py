from django.test import TestCase
from django.test.utils import override_settings
from teaching.models import PublicationStatus
from teaching.factories import CourseMaterialFactory, OrganizationFactory, TagFactory, AuthorFactory
from teaching.serializers import CourseMaterialSerializer


class CourseMaterialSerializerTests(TestCase):

    def setUp(self):
        self.tag1 = TagFactory()
        self.tag2 = TagFactory()
        self.course1 = CourseMaterialFactory(
            tags=(self.tag1, self.tag2)
        )

    def test_retrieve_tags(self):
        serializer = CourseMaterialSerializer(self.course1)
        self.assertIn(self.tag1.label, serializer.data['tags'])
        self.assertIn(self.tag2.label, serializer.data['tags'])
