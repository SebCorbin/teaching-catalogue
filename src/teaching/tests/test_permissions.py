from django.test import TestCase
from django.contrib.auth.models import User, Group

from teaching.models import PublicationStatus
from teaching.factories import CourseMaterialFactory, OrganizationFactory


class PermissionBase(TestCase):
    fixtures = ["groups.json"]

    @classmethod
    def setUpTestData(cls):
        cls.course1 = CourseMaterialFactory(
            status=PublicationStatus.PUBLISHED
        )
        cls.course2 = CourseMaterialFactory()
        cls.orga1 = OrganizationFactory()
        cls.user1 = User.objects.create_user(
            username='camille', email='camille@doe.fr', password='top_secret')
        cls.user2 = User.objects.create_user(
            username='dominique', email='dominique@doe.fr', password='top_secret')
        moderators_group = Group.objects.get(name="Moderators")
        cls.user2.groups.add(moderators_group.id)


class TestAnonymous(PermissionBase):

    def test_anonymous_organization_add(self):
        response = self.client.get('/organization/add')
        self.assertRedirects(response, '/accounts/login/?next=/organization/add')

    def test_anonymous_organization_edit(self):
        response = self.client.get('/organization/1/edit')
        self.assertRedirects(response, '/accounts/login/?next=/organization/1/edit')

    def test_anonymous_coursematerial_add(self):
        response = self.client.get('/coursematerial/add', follow=True)
        self.assertEqual(response.status_code, 200)

    def test_anonymous_coursematerial_edit(self):
        response = self.client.get('/coursematerial/1/edit')
        self.assertRedirects(response, '/accounts/login/?next=/coursematerial/1/edit')

    def test_anonymous_coursematerial_changestatus(self):
        response = self.client.get('/coursematerial/1/changestatus')
        self.assertRedirects(response, '/accounts/login/?next=/coursematerial/1/changestatus')


class TestSimpleUser(PermissionBase):

    def setUp(self):
        success = self.client.login(username="camille", password="top_secret")
        self.assertTrue(success)

    def test_simpleuser_organization_add(self):
        response = self.client.get('/organization/add', follow=True)
        self.assertEqual(response.status_code, 200)

    def test_simpleuser_organization_edit(self):
        response = self.client.get('/organization/1/edit', follow=True)
        self.assertEqual(response.status_code, 403)

    def test_simpleuser_coursematerial_edit(self):
        response = self.client.get('/coursematerial/1/edit', follow=True)
        self.assertEqual(response.status_code, 403)

    def test_simpleuser_coursematerial_changestatus(self):
        response = self.client.get('/coursematerial/1/changestatus', follow=True)
        self.assertEqual(response.status_code, 403)


class TestModeratorUser(PermissionBase):

    def setUp(self):
        success = self.client.login(username="dominique", password="top_secret")
        self.assertTrue(success)

    def test_moderator_organization_add(self):
        response = self.client.get('/organization/add', follow=True)
        self.assertEqual(response.status_code, 200)

    def test_moderator_organization_edit(self):
        response = self.client.get('/organization/1/edit', follow=True)
        self.assertEqual(response.status_code, 200)

    def test_moderator_coursematerial_edit(self):
        response = self.client.get('/coursematerial/1/edit', follow=True)
        self.assertEqual(response.status_code, 200)

    def test_moderator_coursematerial_changestatus(self):
        response = self.client.get('/coursematerial/1/changestatus', follow=True)
        self.assertEqual(response.status_code, 200)
