from django.test import TestCase
from teaching.models import PublicationStatus
from teaching.factories import CourseMaterialFactory, OrganizationFactory


class CourseMaterialModelTest(TestCase):

    def setUp(self):
        self.course1 = CourseMaterialFactory.create(title="First lesson")

    def test_string_display(self):
        self.assertEqual(str(self.course1), "First lesson")

    def test_is_published(self):
        self.course1.status = PublicationStatus.PUBLISHED
        self.course1.save()
        self.assertTrue(self.course1.is_published)


class OrganizationModelTest(TestCase):

    def setUp(self):
        self.orga = OrganizationFactory.create(name="Free skool")

    def test_string_display(self):
        self.assertEqual(str(self.orga), "Free skool")
