from django.test import TestCase
from teaching.models import Author, Organization, Tag
from teaching.factories import AuthorFactory, TagFactory, CourseMaterialFactory, OrganizationFactory
from teaching.forms import CourseMaterialForm


class AddCourseTests(TestCase):

    def setUp(self):
        self.author1 = AuthorFactory.create(name="Jane Doe")
        self.tag1 = TagFactory.create(label='Physics')

    def test_with_new_author(self):
        form = CourseMaterialForm({
            'title': 'My course',
            'new_author': 'Mary Shelley',
        })
        self.assertTrue(form.is_valid())
        course = form.save()
        self.assertEqual('Mary Shelley', course.author.name)

    def test_with_new_orga(self):
        form = CourseMaterialForm({
            'title': 'My course',
            'new_orga': 'Free skool',
        })
        self.assertTrue(form.is_valid())
        course = form.save()
        self.assertEqual('Free skool', course.organization.name)

    def test_with_existing_contributor(self):
        form = CourseMaterialForm({
            'title': 'My course',
            'contributors': ['1'],
        })
        self.assertTrue(form.is_valid())
        course = form.save()
        self.assertIn('Jane Doe', course.contributors.values_list('name', flat=True))

    def test_with_new_contributor(self):
        form = CourseMaterialForm({
            'title': 'My course',
            'contributors': ['Virginia Wolf'],
        })
        self.assertTrue(form.is_valid())
        course = form.save()
        self.assertIn('Virginia Wolf', course.contributors.values_list('name', flat=True))

    def test_with_existing_tag(self):
        form = CourseMaterialForm({
            'title': 'My course',
            'tags': [str(self.tag1.pk)],
        })
        self.assertTrue(form.is_valid())
        course = form.save()
        self.assertIn('Physics', course.tags.values_list('label', flat=True))

    def test_with_new_tag(self):
        form = CourseMaterialForm({
            'title': 'My course',
            'tags': ['Litterature'],
        })
        self.assertTrue(form.is_valid())
        course = form.save()
        self.assertIn('Litterature', course.tags.values_list('label', flat=True))

    def test_with_new_tag_space(self):
        form = CourseMaterialForm({
            'title': 'My course',
            'tags': ['Logiciel Libre'],
        })
        self.assertTrue(form.is_valid())
        course = form.save()
        self.assertIn('Logiciel Libre', course.tags.values_list('label', flat=True))

    def test_with_both_tags(self):
        form = CourseMaterialForm({
            'title': 'My course',
            'tags': ['Litterature', str(self.tag1.pk)],
        })
        self.assertTrue(form.is_valid())
        course = form.save()
        self.assertIn('Litterature', course.tags.values_list('label', flat=True))
        self.assertIn('Physics', course.tags.values_list('label', flat=True))


class UpdateCourseTests(TestCase):

    def setUp(self):
        self.author1 = AuthorFactory.create(name="Jane Doe")
        self.author2 = AuthorFactory.create(name="Camille Claudel")
        self.author3 = AuthorFactory.create(name="Terry Pratchett")
        self.orga1 = OrganizationFactory.create(name="Libre university")
        self.orga2 = OrganizationFactory.create(name="L'école de la rue")
        self.tag1 = TagFactory.create(label='Physics')
        self.tag2 = TagFactory.create(label='Chemical')
        self.course1 = CourseMaterialFactory(
            author=self.author1,
            contributors=(self.author2,),
            tags=(self.tag1,),
            organization=self.orga1
        )

    def test_with_new_author(self):
        form = CourseMaterialForm({
            'title': self.course1.title,
            'author': str(self.course1.author.pk),
            'contributors': [str(v.pk) for v in self.course1.contributors.all()],
            'new_author': 'Mary Shelley',
            'tags': [str(v.pk) for v in self.course1.tags.all()],
            'organization': str(self.course1.organization.pk),
        }, instance=self.course1)
        self.assertTrue(form.is_valid())
        course = form.save()
        self.assertEqual('Mary Shelley', course.author.name)
        self.assertIn('Camille Claudel', course.contributors.values_list('name', flat=True))

    def test_with_new_orga(self):
        form = CourseMaterialForm({
            'title': self.course1.title,
            'author': str(self.course1.author.pk),
            'contributors': [str(v.pk) for v in self.course1.contributors.all()],
            'new_author': 'Mary Shelley',
            'tags': [str(v.pk) for v in self.course1.tags.all()],
            'organization': str(self.course1.organization.pk),
            'new_orga': 'Free skool',
        }, instance=self.course1)
        self.assertTrue(form.is_valid())
        course = form.save()
        self.assertEqual('Free skool', course.organization.name)

    def test_with_existing_contributor(self):
        form = CourseMaterialForm({
            'title': self.course1.title,
            'author': str(self.course1.author.pk),
            'contributors': [str(v.pk) for v in self.course1.contributors.all()] + [str(self.author3.pk)],
            'tags': [str(v.pk) for v in self.course1.tags.all()],
            'organization': str(self.course1.organization.pk),
        }, instance=self.course1)
        self.assertTrue(form.is_valid())
        course = form.save()
        self.assertIn('Camille Claudel', course.contributors.values_list('name', flat=True))
        self.assertIn('Terry Pratchett', course.contributors.values_list('name', flat=True))

    def test_with_new_contributor(self):
        form = CourseMaterialForm({
            'title': self.course1.title,
            'author': str(self.course1.author.pk),
            'contributors': [str(v.pk) for v in self.course1.contributors.all()] + ['Virginia Wolf'],
            'tags': [str(v.pk) for v in self.course1.tags.all()],
            'organization': str(self.course1.organization.pk),
        })
        self.assertTrue(form.is_valid())
        course = form.save()
        self.assertIn('Camille Claudel', course.contributors.values_list('name', flat=True))
        self.assertIn('Virginia Wolf', course.contributors.values_list('name', flat=True))

    def test_add_existing_tag(self):
        form = CourseMaterialForm({
            'title': self.course1.title,
            'author': str(self.course1.author.pk),
            'contributors': [str(v.pk) for v in self.course1.contributors.all()],
            'tags': [str(v.pk) for v in self.course1.tags.all()] + [str(self.tag2.pk)],
            'organization': str(self.course1.organization.pk),
        })
        self.assertTrue(form.is_valid())
        course = form.save()
        self.assertIn('Physics', course.tags.values_list('label', flat=True))
        self.assertIn('Chemical', course.tags.values_list('label', flat=True))

    def test_add_new_tag(self):
        form = CourseMaterialForm({
            'title': 'My course',
            'tags': [str(v.pk) for v in self.course1.tags.all()] + ['Litterature'],
        })
        self.assertTrue(form.is_valid())
        course = form.save()
        self.assertIn('Physics', course.tags.values_list('label', flat=True))
        self.assertIn('Litterature', course.tags.values_list('label', flat=True))

    def test_with_both_tags(self):
        form = CourseMaterialForm({
            'title': 'My course',
            'tags': [str(v.pk) for v in self.course1.tags.all()] + ['Litterature', str(self.tag2.pk)],
        })
        self.assertTrue(form.is_valid())
        course = form.save()
        self.assertIn('Litterature', course.tags.values_list('label', flat=True))
        self.assertIn('Physics', course.tags.values_list('label', flat=True))
        self.assertIn('Chemical', course.tags.values_list('label', flat=True))
