from rest_framework import serializers
from . import models


class AuthorSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)

    class Meta:
        model = models.Author
        fields = ['id', 'name']


class OrganizationSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Organization
        fields = ['name', 'description', 'website', 'city']


class CourseMaterialSerializer(serializers.ModelSerializer):
    author = serializers.StringRelatedField()
    tags = serializers.StringRelatedField(many=True)
    level = serializers.StringRelatedField()
    resource_license = serializers.StringRelatedField()
    organization = serializers.StringRelatedField()

    class Meta:
        model = models.CourseMaterial
        fields = ['title', 'description', 'course_url', 'author', 'tags', 'level', 'resource_license', 'organization', 'status']
