from django.shortcuts import render
from django.urls import reverse_lazy
from django.views import generic

from accounts.forms import SignUpForm


class Signup(generic.CreateView):
    """Signup view"""

    form_class = SignUpForm
    success_url = reverse_lazy('login')
    template_name = 'registration/signup.html'
