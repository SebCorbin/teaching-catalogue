DEBUG = True

ALLOWED_HOSTS = ['*']

SITE_TITLE = "Ressources pédagogiques pour l'éducation populaire au numérique"
